var moment = require('moment')

const state = {
  startDateTime: null,
  endDateTime: null,
  exercises: [],
  plannedWorkout: {
    duration: 45,
    startDateTime: new Date(2018, 1, 6, 8, 0, 0, 0)
  },
  currentExercise: {
    startDateTime: null,
    endDateTime: null
  }
}

const getters = {
  startDateTime: state => {
    return state.startDateTime
  },
  endDateTime: state => {
    return state.endDateTime
  },
  exercises: state => {
    return state.exercises
  },
  plannedWorkout: state => {
    return state.plannedWorkout
  },
  currentExercise: state => {
    return state.currentExercise
  },
  ceStartDateTime: state => {
    return state.currentExercise.startDateTime
  },
  ceEndDateTime: state => {
    return state.currentExercise.endDateTime
  }
}

const mutations = {
  SET_STARTDATETIME (state) {
    state.startDateTime = moment()
  },
  SET_ENDDATETIME (state) {
    state.endDateTime = moment()
  },
  PUT_EXERCISE (state, exercise) {
    state.exercises.push(exercise)
  },
  SET_PLANNEDWORKOUT (state, workout) {
    state.plannedWorkout = workout
  },
  SET_CE_STARTDATETIME (state) {
    state.currentExercise.startDateTime = moment()
  },
  SET_CE_ENDDATETIME (state) {
    state.currentExercise.endDateTime = moment()
  }
}

const actions = {
  setWorkoutStart ({ commit }) {
    commit('SET_STARTDATETIME')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
