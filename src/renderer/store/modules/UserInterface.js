const state = {
  showTimers: false
}

const getters = {
  showTimers: state => {
    return state.showTimers
  }
}
const mutations = {
  SHOW_TIMERS (state) {
    state.showTimers = true
  },
  HIDE_TIMERS (state) {
    state.showTimers = false
  }
}

const actions = {
  someAsyncTask ({ commit }) {
    // do something async
    commit('INCREMENT_MAIN_COUNTER')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
