import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'WelcomeView_Main',
      component: require('@/components/WelcomeView/_Main').default
    },
    {
      path: '/inspire',
      name: 'InspireView_Main',
      component: require('@/components/InspireView/_Main').default
    },
    {
      path: '/workout-prep',
      name: 'WorkoutPrepView_Main',
      component: require('@/components/WorkoutPrepView/_Main').default
    },
    {
      path: '/exercise',
      name: 'ExerciseView_Main',
      component: require('@/components/ExerciseView/_Main').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
